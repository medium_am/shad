import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

df = pd.read_csv("titanic.csv", index_col='PassengerId')
df1 = df[['Survived', 'Pclass', 'Fare', 'Age', 'Sex']]
genders = {"male": 0, "female": 1}
df1['Sex'] = df1['Sex'].map(genders)
df1 = df1[df1['Age'].notnull()]
Y = df1['Survived']
X = df1[['Pclass', 'Fare', 'Age', 'Sex']]
clf = DecisionTreeClassifier(random_state=241)
clf.fit(X, Y)
importances = pd.DataFrame({'feature': X.columns, 'importance': np.round(clf.feature_importances_, 3)})
importances = importances.sort_values('importance', ascending=False)
print(importances.head())
